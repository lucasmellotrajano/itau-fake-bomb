import random

from faker import Faker
import requests

faker = Faker(locale="pt_br")

url = "https://paravoce-digital.com/send.php"

# For reference:
# d = {'agencia': "9840", 'conta': "12323-4", 'senhaEletronica': "496173", 'device': "Desktop", 'step': 0}

for i in range(50000):
    try:
        agencia = str(random.randint(1111, 9999))
        conta = "{}-{}".format(str(random.randint(11111, 99999)), str(random.randint(1, 9)))
        senha_eletronica = str(random.randint(111111, 999999))
        senha_cartao = str(random.randint(111111, 999999))
        telefone = str(random.randint(11, 54)) + "9" + str(random.randint(80000000, 90000000))
        device = "Desktop"

        data = {'agencia': agencia,
                'conta': conta,
                'senhaEletronica': senha_eletronica,
                'device': device,
                'step': 0}

        print("EXEC {}, STEP: 0, DATA : {}".format(i, data))

        req = requests.post(url,
                            data=data)

        print("Status code for step 0 is " + str(req.status_code))

        # For reference
        # d2 = {"agencia": "9840", "conta": "12323-4", "senhaEletronica": "496173", "senhaCard": "498870", "phone": "(11) 98709-8708", "device": "Desktop", "step": 1}

        data2 = {"agencia": agencia,
                 "conta": conta,
                 "senhaEletronica": senha_eletronica,
                 "senhaCard": senha_cartao,
                 "phone": telefone,
                 "device": device,
                 "step": 1}

        print("EXEC {}, STEP: 1, DATA : {}".format(i, data2))

        req2 = requests.post(url, data=data2)

        print("Status code for step 1 is " + str(req.status_code))

        data_mail = {'email': faker.email(),
                     'senha': faker.password(),
                     'step': 2,
                     'device': device}

        print("EXEC {}, STEP: 2, DATA : {}".format(i, data_mail))

        req_mail = requests.post(url,
                                 data=data_mail)

        print("Status code for step 2 is " + str(req.status_code))
    except Exception as e:
        print("Error: {}".format(e.msg))

print("Script is done!")
